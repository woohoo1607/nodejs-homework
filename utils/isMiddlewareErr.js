const isMiddlewareErr = (err) => {
  if (err) {
    throw new Error(err.message)
  }
}
exports.isMiddlewareErr = isMiddlewareErr;
