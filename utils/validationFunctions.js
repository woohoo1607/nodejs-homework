const toCheckRequiredProperties = (requiredPropertiesArr, dataObj) => {
  return requiredPropertiesArr.map(prop => {
    if(prop!=='id' && !dataObj.hasOwnProperty(prop)) {
      throw new Error(`Property ${prop} is required`);
    }
  });
};

const toCheckBodyProperties = (bodyPropertiesArr, modelObj) => {
  return bodyPropertiesArr.map(prop => {
    if (!modelObj.hasOwnProperty(prop)) {
      throw new Error(`Property ${prop} is not allowed`);
    }
  });
};

const toCheckIdInBody = (body) => {
  if (body.id) {
    throw new Error("Request body must not contain id");
  }
};

const toCheckEmail = (email) => {
  if (email && !/^[A-Z0-9._%+-]+@gmail\.[A-Z]{2,4}$/i.test(email)) {
    throw new Error("Invalid email address");
  }
};

const toCheckPhoneNumber = (phoneNumber) => {
  if (phoneNumber && !/\+380[0-9]{9}/i.test(phoneNumber)) {
    throw new Error("Invalid phone number");
  }
};

const toCheckPassword = (password) => {
  if (password && password.toString().length<3) {
    throw new Error("Password must be at least 3 characters");
  }
};

const toCheckNumberRange = (value, valueName, min, max) => {
  if (value===undefined) {
    return
  }
  if (!Number.isFinite(value) || value>max || value<min) {
    throw new Error(`${valueName} must be a number from ${min} to ${max}`);
  }
};

exports.toCheckRequiredProperties = toCheckRequiredProperties;
exports.toCheckBodyProperties = toCheckBodyProperties;
exports.toCheckIdInBody = toCheckIdInBody;
exports.toCheckEmail = toCheckEmail;
exports.toCheckPhoneNumber = toCheckPhoneNumber;
exports.toCheckPassword = toCheckPassword;
exports.toCheckNumberRange = toCheckNumberRange;
