const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  search(search) {
    const item = FighterRepository.getOne(search);
    if(!item) {
      throw new Error("Fighter is not found");
    }
    return item;
  }

  write(data) {
    const fighter = FighterRepository.create(data);
    if(!fighter) {
      throw new Error("Fighter is not created");
    }
    return fighter;
  }

  getFighters() {
    const fighters = FighterRepository.getAll();
    if(fighters.length===0) {
      throw new Error("Fighters is not found");
    }
    return fighters;
  }

  updateFighter(id, data) {
    const fighter = FighterRepository.update(id,data);
    if(!fighter) {
      throw new Error("Fighter is not found");
    }
    return fighter;
  }

  deleteFighter(id) {
    const fighter = FighterRepository.delete(id);
    if(fighter.length===0) {
      throw new Error("Fighter is not found");
    }
    return fighter[0];
  }
}

module.exports = new FighterService();
