const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            throw new Error("User is not found");
        }
        return item;
    }

    write(data) {
        data.password = data.password.toString();
        const user = UserRepository.create(data);
        if(!user) {
            throw new Error("User is not created");
        }
        return user;
    }

    getUsers() {
        const users = UserRepository.getAll();
        if(users.length===0) {
            throw new Error("Users is not found");
        }
        return users;
    }

    updateUser(id, data) {
        const user = UserRepository.update(id,data);
        if(!user) {
            throw new Error("User is not found");
        }
        return user;
    }

    deleteUser(id) {
        const user = UserRepository.delete(id);
        if(user.length===0) {
            throw new Error("User is not found");
        }
        return user[0];
    }
}

module.exports = new UserService();
