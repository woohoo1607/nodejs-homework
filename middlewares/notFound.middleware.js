const notFoundMiddleware = (req, res, next) => {
    res.status(404).json({
        error: true,
        message: "Page not found",
    })

};

exports.notFoundMiddleware = notFoundMiddleware;
