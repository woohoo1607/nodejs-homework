const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
    if (res.err) {
        const errMsgArray = res.err.message.split(" ");
        const length = errMsgArray.length;
        if (errMsgArray[length-1]==="found") {
            res.status(404).json({
                error: true,
                message: res.err.message,
            })
        } else {
            res.status(400).json({
                error: true,
                message: res.err.message,
            });
        }
    } else {
        res.status(200).json(res.data);
    }

}

exports.responseMiddleware = responseMiddleware;
