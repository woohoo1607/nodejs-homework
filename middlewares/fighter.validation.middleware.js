const { fighter } = require('../models/fighter');
const {
    toCheckBodyProperties,
    toCheckIdInBody,
    toCheckRequiredProperties, toCheckNumberRange
} = require('../utils/validationFunctions');

const createFighterValid = (req, res, next) => {
    try {
        req.body.health = fighter.health;
        req.body.power ? req.body.power = +req.body.power : undefined;
        req.body.defense ? req.body.defense = +req.body.defense : undefined;

        toCheckRequiredProperties(Object.keys(fighter), req.body);
        toCheckBodyProperties(Object.keys(req.body), fighter);
        toCheckNumberRange(req.body.power, "Power", 1,99);
        toCheckNumberRange(req.body.defense, "Defense", 1,10);
        toCheckIdInBody(req.body);

    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

const updateFighterValid = (req, res, next) => {
    try {
        req.body.health = fighter.health;
        req.body.power ? req.body.power = +req.body.power : undefined;
        req.body.defense ? req.body.defense = +req.body.defense : undefined;

        toCheckBodyProperties(Object.keys(req.body), fighter);
        toCheckNumberRange(req.body.power, "Power", 1,99);
        toCheckNumberRange(req.body.defense, "Defense", 1,10);
        toCheckIdInBody(req.body);

    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
