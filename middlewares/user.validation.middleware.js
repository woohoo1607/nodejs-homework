const { user } = require('../models/user');
const {
    toCheckBodyProperties,
    toCheckEmail,
    toCheckIdInBody, toCheckPassword, toCheckPhoneNumber,
    toCheckRequiredProperties
} = require('../utils/validationFunctions');

const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    try {
        toCheckRequiredProperties(Object.keys(user), req.body);
        toCheckBodyProperties(Object.keys(req.body), user);
        toCheckIdInBody(req.body);
        toCheckEmail(req.body.email);
        toCheckPhoneNumber(req.body.phoneNumber);
        toCheckPassword(req.body.password);

    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

const updateUserValid = (req, res, next) => {
    try {
        toCheckBodyProperties(Object.keys(req.body), user);
        toCheckIdInBody(req.body);
        toCheckEmail(req.body.email);
        toCheckPhoneNumber(req.body.phoneNumber);
        toCheckPassword(req.body.password);

    } catch (err) {
        res.err = err;
    } finally {
        next();
    }
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
